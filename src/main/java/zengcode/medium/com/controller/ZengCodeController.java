package zengcode.medium.com.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import zengcode.medium.com.model.Person;

@RestController
@RequestMapping("/zengcode")
public class ZengCodeController {

    @ApiOperation(value = "Response the variable name", response = String.class)
    @RequestMapping(value = "/show/name/{name}", method= RequestMethod.GET)
    public String showName(@PathVariable String name){

        return "Your name is " + name;
    }

    @ApiOperation(value = "Response the variable name and age ", response = String.class)
    @RequestMapping(value = "/show/name/{name}/age/{age}", method= RequestMethod.GET)
    public String showNameAge(@PathVariable("name") String name, @PathVariable("age") int age){

        return "Your name is " + name + ", you are " + age + " years old.";
    }


    @ApiOperation(value = "Response Person object ", response = Person.class)
    @RequestMapping(value = "/show/person", method= RequestMethod.PUT)
    public Person showPerson(@RequestBody Person person){

        person.setFirstName("Your name is " + person.getFirstName());
        return person;
    }

}
